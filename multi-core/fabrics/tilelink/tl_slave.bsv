package tl_slave;

  import StmtFSM :: * ;
  import GetPut::*;
  import FIFO::*;
  import FIFOF::*;
  import SpecialFIFOs :: * ;
  import Semi_FIFOF::*;
  import LFSR::*;
  import Tilelink_Types::*;
  //import Tilelink_NIC::*;
  /*
    Accepts only one Acquire Request at a time , Blocking 
    Listens Request on A Channel
    Generates Requests on B Channel 
    Waits for Probe responses/Release on C Channel
    Sends Response on D channel
    Waits on E Channel
    Done using Statement FSM

  Across the five channels, TL-C specifies ten messages comprising three operations.
    Message     Opcode      Operation     A   B   C   D   E     Response
    Acquire        6         Acquire      y   .   .   .   .     Grant, GrantData
    Grant          4         Acquire      .   .   .   y   .     GrantAck
    GrantData      5         Acquire      .   .   .   y   .     GrantAck
    GrantAck       -         Acquire      .   .   .   .   y
    Probe          6         Probe        .   y   .   .   .     ProbeAck, ProbeAckData
    ProbeAck       4         Probe        .   .   y   .   .
    ProbeAckData   5         Probe        .   .   y   .   .
    Release        6         Release      .   .   y   .   .     ReleaseAck
    ReleaseData    7         Release      .   .   y   .   .     ReleaseAck
    ReleaseAck     6         Release      .   .   .   y   .
    Table 8.2: Summary of TL-C Permission Transfer Operation Messages

  */

  interface Ifc_tl_slave_tg#(numeric type a, numeric type w, numeric type z ,numeric type o ,numeric type i);
    method Action ma_start;
    method Action ma_finish;
    method Action ma_rand_value(Bit#(a) lfsr_value);
    interface Ifc_tlc_slave#(a,w,z,o,i) tl_slave;
    method Bool mv_done;
  endinterface
  
  (*synthesize*)
  module mk_instance_tl_slave_tg(Empty);
    Ifc_tl_slave_tg#(32,64,32,2,2) instant <-  mk_tl_slave_tg(0,TL_C);
  endmodule

  module mk_tl_slave_tg#(Integer source_id ,TL_Capability capability)(Ifc_tl_slave_tg#(a,w,z,o,i))
  provisos(Add#(a__, z, a));

    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) rg_finish <- mkReg(False);
   
    // Slave Agent
    Ifc_tlc_slave_agent#(a,w,z,o,i) tl_slave_agent <- mktlc_slave_agent();

    LFSR#(Bit#(4))  rand_event <- mkLFSR_4();

    Reg#(Bit#(z)) rg_beat_counter <- mkReg(0);

    FIFO#(Bit#(a)) fifo_txn_signature <- mkSizedFIFO(2);

    Reg#(Bit#(o)) rg_acquire_source <- mkReg(unpack(0));
    Reg#(Bit#(z)) rg_acquire_size    <- mkReg(unpack(0));
    Reg#(Bit#(z)) rg_acquire_serviced<- mkReg(unpack(0));
    Reg#(Bit#(o)) rg_release_source <- mkReg(unpack(0));
    Reg#(Bit#(z)) rg_release_size    <- mkReg(unpack(0));

    Reg#(Bool) rg_acquire_block <- mkReg(False);
    Reg#(Bool) rg_start_a <- mkReg(False);
		Reg#(Bool) rg_start_d <- mkReg(False);
		Reg#(Bool) rg_start_r <- mkReg(False);
		Reg#(Bool) rg_start_rd <- mkReg(False);
		Reg#(Bool) rg_start_e <- mkReg(False);

    rule rl_reset_acquire_block ( ! rg_acquire_block );
      rg_start_a <= True;
      rg_acquire_block <= True;
    endrule

    rule rl_drive_A (rg_start_a == True);
      A_channel#(a,w,z,o) packet_acquire = tl_slave_agent.o_a_channel.first();
      tl_slave_agent.o_a_channel.deq();
      rg_acquire_size   <= packet_acquire.a_size;
      rg_acquire_source <= packet_acquire.a_source;
      rg_acquire_serviced <= 0;
      $display("%d\t\t",source_id,fshow(packet_acquire));
      rg_start_a <= False;
      rg_start_d <= True;
    endrule
    // Concurrent C - Channel handling 
    // C- Channel Release
    // C -Channel Probe Acknolowdgement Handling
    rule rl_drive_C;
      //tl_slave_agent.o_c_channel
      // look at pending blocking operation request bits.
      // Release in progress. - block till probe ack.
      // Probe Ack Data in progress - delay and wait on release. 
    endrule

    rule rl_drive_E (rg_start_e == True);
      E_channel#(i) grant_ack = tl_slave_agent.o_e_channel.first();
      tl_slave_agent.o_e_channel.deq();
      rg_acquire_block <= False;
      rg_start_e <= False;
    endrule

    // Concurrent D - Channel handling
    // D- Channel release response
    // D- Channel grant   response
    rule rl_drive_D(rg_start_d == True);
      // dequeue - look at opcode - update relevant counter
      D_channel#(w,z,o,i) grant_packet = ?;
      grant_packet.d_opcode = GrantData;
      grant_packet.d_source = rg_acquire_source;
      grant_packet.d_size = rg_acquire_size;
      grant_packet.d_sink = fromInteger(source_id);
      tl_slave_agent.i_d_channel.enq(grant_packet);
      if(rg_acquire_serviced < rg_acquire_size)
        rg_acquire_serviced <= rg_acquire_serviced +1;
      else begin
        rg_start_e <= True;
        rg_start_d <= False;
        end
    endrule
    
    // B- Channel
    // tl_slave_agent.i_b_channel

    // // Start
    Reg#(Bit#(4)) rg_event <- mkReg(0);

    rule rl_activity(rg_start && (!rg_finish)); 
      rg_event <= rand_event.value();
    //   rand_event.next();
    endrule
    
    // rule rl_acquire(rg_event[0] == 1'b1);
    //   fsm_grant.start();
    // endrule
    // rule rl_probe_dirty(rg_event[1] == 1'b1);
    //   fsm_probe.start();
    // endrule
    // rule rl_release(rg_event[2] == 1'b1);
    //   fsm_release_ack.start();
    // endrule

    // Finish

    method Action ma_start if(!rg_start);
      rg_start <= True;
      rand_event.seed(4'hf);
    endmethod
    method Action ma_finish if (rg_start);
      rg_finish <= True;
    endmethod
    method Action ma_rand_value(Bit#(a) lfsr_value) if(rg_start);
      fifo_txn_signature.enq(lfsr_value); 
    endmethod
    method Bool mv_done if (rg_finish);
      return True;
    endmethod

    interface tl_slave = tl_slave_agent.tilelink_side;
  endmodule
endpackage :tl_slave