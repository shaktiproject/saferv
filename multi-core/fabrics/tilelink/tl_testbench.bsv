package tl_testbench;
  
  import tl_master::*;
  import tl_slave::*;
  import StmtFSM :: * ;
  import GetPut::*;
  import FIFO::*;
  import FIFOF::*;
  import SpecialFIFOs :: * ;
  import LFSR::*;
  import Vector::*;

  import Tilelink::*;
  import Tilelink_Types::*;
  import Tilelink_Mesh_Fabric::*;
  import Connectable::*;
  import address_map::*;

  `define NUM_Masters 4
  `define NUM_Slaves 4

  (*synthesize*)
  module mk_tilelink_tg_testbench(Empty);
    
    Reg#(Bit#(32)) rg_cycle <- mkReg(0);
    Reg#(Bool) rg_start <- mkReg(False);
    Wire#(Bit#(32)) wr_rand_value <- mkWire();

    Vector#(`NUM_Masters,Ifc_tl_master_tg#(32, 8,4,TLog#(`NUM_Masters),TLog#(`NUM_Slaves))) v_master_tg;
    Vector#(`NUM_Slaves,Ifc_tl_slave_tg#(32,8,4,TLog#(`NUM_Masters),TLog#(`NUM_Slaves))) v_slave_tg;
    for (Integer i=0; i<`NUM_Masters; i=i+1)begin
      v_master_tg[i] <- mk_tl_master_tg(fromInteger(i),TL_C);
    end
    for (Integer i=0; i<`NUM_Slaves; i=i+1)begin
      v_slave_tg[i] <- mk_tl_slave_tg(fromInteger(i),TL_C);
    end
    
    // Random num gen 
    LFSR#(Bit#(32)) rand_txn_signature <- mkLFSR_32();
    
    rule rl_initial(rg_start == False);
      rg_start <= True;
      rand_txn_signature.seed(32'hBEBECAFE);
    endrule
    rule rl_next_txn(rg_start==True);
      wr_rand_value <= rand_txn_signature.value();
      rand_txn_signature.next();
    endrule

    for (Integer i=0; i<`NUM_Masters; i=i+1)begin
      rule rl_start_m(rg_start == True);
        v_master_tg[i].ma_start();
      endrule
      rule rl_drive_random_m(rg_start == True );
        v_master_tg[i].ma_rand_value(wr_rand_value);
      endrule
      rule rl_finish_m(rg_cycle == 'd1000);
        v_master_tg[i].ma_finish();
      endrule
    end
    for (Integer i=0; i<`NUM_Slaves; i=i+1)begin
      rule rl_start_s(rg_start == True);
        v_slave_tg[i].ma_start();
      endrule
      rule rl_drive_random_s(rg_start == True );
        v_slave_tg[i].ma_rand_value(wr_rand_value);
      endrule
      rule rl_finish_s(rg_cycle == 'd1000);
        v_slave_tg[i].ma_finish();
      endrule
    end
    rule rl_exit_sim(v_master_tg[0].mv_done && v_slave_tg[0].mv_done);
      // Exits on notification that the testbench has finished transacting all packets FIXME
      $display("End of Simulation Cycles! ");
      $finish();
    endrule

    rule rl_sim_time;
      rg_cycle <= rg_cycle + 1;
    endrule

    // Tile Link Mesh Fabric
    Vector#(`NUM_Masters,Vector#(`NUM_Slaves,Bool)) route_all = replicate(replicate(True));
    Ifc_Tilelink_Fabric#(32,8,4,TLog#(`NUM_Masters),TLog#(`NUM_Slaves),`NUM_Masters,`NUM_Slaves) tl_mesh_fabric <- mkTilelink_Mesh_Fabric(address_map, route_all ,TL_C )();
    
    for (Integer i=0; i<`NUM_Masters; i=i+1)begin
      mkConnection(tl_mesh_fabric.v_master_port[i],v_master_tg[i].tl_master);
    end
    for (Integer i=0; i<`NUM_Slaves; i=i+1)begin
      mkConnection(tl_mesh_fabric.v_slave_port [i], v_slave_tg[i].tl_slave );
    end

    // // Tile Link Network on Chip  Fabric
    

  endmodule  

endpackage : tl_testbench