package ShaktiLink_NIC;

`include "system.defines"
import GetPut ::*;
import FIFO ::*;
import FIFOF::*;
import SpecialFIFOs ::*;
import Semi_FIFOF::*;
import Connectable ::*;
import Vector::*;

import MessageTypes ::*;
import Types ::*;
import RoutingTypes ::*;
import CreditUnit::*;
import SmartVCAllocUnit::*;
import CreditTypes::*;
import Network::*;
import NIC_interface::*;
//import StatLogger::*;

import ShaktiLink_Types::*;
import address_map ::*;
`include "Logger.bsv"
typedef enum{
ShaktiLink_Req_channel,
ShaktiLink_Fwd_channel,
ShaktiLink_Resp_channel,
SysManagement } VirtualNetworks deriving (Bits , Eq , FShow);

typedef struct{
	Len node;
	Bit#(a) address;
	Bit#(w) data;
  Bool operation;
} Sys_mon_type#(numeric type a,numeric type w) deriving (Bits,Eq,FShow);

interface ShaktiLink_NoC_Endpoint#(numeric type a, numeric type w, numeric type o,
numeric type i, numeric type op, numeric type acks, numeric type u);
	interface Ifc_slc_master #(a,w,o,i,op,acks,u) node_master;
	interface Ifc_slc_slave  #(a,w,o,i,op,acks,u) node_slave ;
	interface Get#(Sys_mon_type#(a,w)) sysmon_out;
	interface Put#(Sys_mon_type#(a,w)) sysmon_in;
	method Action injection_rate_governor(Bool rules_can_fire);
	method Action local_criticality (Bool criticality);
endinterface

(* descending_urgency = "rl_transfer_m_D,rl_transfer_m_B,rl_transfer_s_A,rl_transfer_s_SysMon"*)
(* descending_urgency = "rl_transfer_s_D,rl_transfer_s_B,rl_transfer_m_A,rl_transfer_m_SysMon"*)
module mkConnect_ShaktiLink_NOC#(NetworkOuterInterface mesh,Integer node_id_y , Integer node_id_x )(ShaktiLink_NoC_Endpoint#(a,w,o,i,op,acks,u))
												provisos(
													Add#(1, a__, o),
													Add#(1, b__, i),
													Add#(i,0,o),
													Add#(c__, TAdd#(op, TAdd#(o, TAdd#(o, TAdd#(a, TAdd#(w, TAdd#(TMul#(w, 8),TAdd#(1, u))))))), 132),
													Add#(e__, TAdd#(6, TAdd#(a, TAdd#(w, 1))), 132),
													Add#(f__, TAdd#(op, TAdd#(8, TAdd#(3, TAdd#(1, TAdd#(o, TAdd#(i, TAdd#(a,TAdd#(w, TAdd#(TMul#(w, 8), u))))))))), 132),
													Add#(g__, TAdd#(op, TAdd#(acks, TAdd#(1, TAdd#(o, TAdd#(i, TAdd#(a,TAdd#(TMul#(w, 8), TAdd#(1, TAdd#(1, u))))))))), 132)
												);// TODO :: Rewrite Provisos to Human Readable and Not Configuration Sensitive

		// Transactors
    Ifc_slc_master_agent#(a,w,o,i,op,acks,u) sl_master_agent<- mkslc_master_agent();
		Ifc_slc_slave_agent #(a,w,o,i,op,acks,u) sl_slave_agent <- mkslc_slave_agent ();

		Reg#(Bit#(1)) crit <- mkReg(0);
		Wire#(Bool) rules_can_fire <- mkDWire(False);

		FIFOF#(Sys_mon_type#(a,w)) fifo_m_sys_mon <- mkFIFOF1();
		FIFOF#(Sys_mon_type#(a,w)) fifo_s_sys_mon <- mkFIFOF1();
    FIFOF#(Flit)                                 buf_flit      <- mkBypassFIFOF;
    FIFOF#(MsgType)                              buf_msgType      <- mkBypassFIFOF;
    Wire#(Bool)    wrA <- mkDWire(False);
    Wire#(Bool)    wrB <- mkDWire(False);
    Wire#(Bool)    wrC <- mkDWire(False);
    Wire#(Bool)    injA <- mkDWire(False);
    Wire#(Bool)    injB <- mkDWire(False);
    Wire#(Bool)    injC <- mkDWire(False);
    Wire#(Flit)    wr_flitinj <- mkWire;
		////// - Credit & Credit Link Management

		SmartVCAllocUnit   vcAllocUnit 	<- mkSmartVCAllocUnit;
		ReverseCreditUnit  creditUnit <- mkReverseCreditUnit;
		Integer max_r_fifo_depth = 32;// Maximum number of outstanding Request flits for a channel which are not being serviced +1
		//Integer max_r_fifo_depth = valueOf(TAdd#(TExp#(o),0));// Maximum number of outstanding Request flits for a channel which are not being serviced + 1
		Vector#(NumVNETs,FIFOF#(Flit)) f_recieve_flit <- replicateM(mkSizedBypassFIFOF(max_r_fifo_depth));

		// *think* : in which direction is data/credit is moving!
		// credit unit used during ejection of flit
		// data moves from router(local side) to NIC
		// credit moves from NIC to router (local)
		// credit updates the free VC pool used for ejection
		rule rl_input_credit_connect;
			let credit <- creditUnit.getCredit;
			mesh.putCredit(credit);
		endrule

		// credit unit of local input port
		// data moves from NIC to router (local)
		// credit moves from router(local) to NIC
		// credit updates the free VC Pool used for injection
		rule rl_output_credit_connect;
			CreditSignal credit <- mesh.getCredit;
			if(isValid(credit))begin
					vcAllocUnit.putFreeVC(validValue(credit).msgType,validValue(credit).critType, validValue(credit).vc);
			end
		endrule

		////// Data & Data Link Management

		//Shakti channel A
		rule rl_transfer_m_A(rules_can_fire);
			Req_channel#(a,w,o,i,op,u) lv_flit_data = sl_slave_agent.o_req_channel.first();
			Bit#(DataSz) data = zeroExtend(pack(lv_flit_data));
			// Address to Destination
			Bit#(o) sl_dest = req_move_from_mi_to_sj(lv_flit_data.address,lv_flit_data.dest);
			Len dest_coord = fn_slave_to_coordinates(sl_dest);
			LookAheadRouteInfo routeInfo = fn_encode_routing_info( Len {x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) },dest_coord );
			//$display(fshow(sl_dest),fshow(dest_coord),fshow(routeInfo),fshow(Len {x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) }));
      let lv_crit = lv_flit_data.user;
      //$display("lv_crit is: %b ", lv_crit);
			Flit flit = Flit { msgType : pack(ShaktiLink_Req_channel),
													critType : (lv_crit[0]),
													vc : ?,
													flitType : HeadTail,
													routeInfo : routeInfo,
													flitData : data};
			//flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType );
			//mesh.putFlit(flit);
	    `logLevel( fabric, 0, $format("", fshow (lv_flit_data)))
     //crit <= crit+1;
     HasVC hasVC = vcAllocUnit.hasVC;
     if(hasVC[flit.critType][pack(ShaktiLink_Req_channel)] == 1)begin
       sl_slave_agent.o_req_channel.deq();
       injA <= True;
       wr_flitinj <= flit;
     end
  endrule

		// Shakti channel B
		rule rl_transfer_s_B(rules_can_fire);
			Fwd_channel#(a,w,o,i,op,u) 	lv_flit_data = sl_master_agent.o_fwd_channel.first();
			Bit#(DataSz) data = zeroExtend(pack(lv_flit_data));
			// dest to coords
			Bit#(i) sl_dest = fwd_move_from_sj_to_mi(lv_flit_data.address,lv_flit_data.dest);
			Len dest_coord = fn_master_to_coordinates(sl_dest);
			LookAheadRouteInfo routeInfo = fn_encode_routing_info( Len {	x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) },dest_coord);

      let lv_crit = lv_flit_data.user;
      Flit flit = Flit { msgType : pack(ShaktiLink_Fwd_channel),
													critType : lv_crit[0],
													vc : ?,
													flitType : HeadTail,
													routeInfo : routeInfo,
													flitData : data};
			//flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType);
			//mesh.putFlit(flit);
			//sl_master_agent.o_fwd_channel.deq();
	
	    `logLevel( fabric, 0, $format("", fshow (lv_flit_data)))
      HasVC hasVC = vcAllocUnit.hasVC;
      if(hasVC[flit.critType][pack(ShaktiLink_Fwd_channel)] == 1)begin
       sl_master_agent.o_fwd_channel.deq();
       injB <= True;
       wr_flitinj <= flit;
     end
		endrule

		// Shakti channel D
		rule rl_transfer_s_D(rules_can_fire);
			Resp_channel#(a,w,o,i,op,acks,u) lv_flit_data = sl_master_agent.o_resp_channel.first();
			Bit#(DataSz) data = zeroExtend(pack(lv_flit_data));
			// dest to coords
			Bit#(i) sl_dest = resp_move_from_sj_to_mi(lv_flit_data.address,lv_flit_data.dest);
			Len dest_coord = fn_master_to_coordinates(sl_dest);
			LookAheadRouteInfo routeInfo = fn_encode_routing_info( Len {	x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) },dest_coord);

      let lv_crit = lv_flit_data.user;
      Flit flit = Flit { msgType : pack(ShaktiLink_Resp_channel),
													critType : lv_crit[0],
													vc : ?,
													flitType : HeadTail,
													routeInfo : routeInfo,
													flitData : data};
			//flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType);
			//mesh.putFlit(flit);
			//sl_master_agent.o_resp_channel.deq();
			
	        `logLevel( fabric, 0, $format("", fshow (lv_flit_data)))
      HasVC hasVC = vcAllocUnit.hasVC;
      if(hasVC[flit.critType][pack(ShaktiLink_Resp_channel)] == 1)begin
       sl_master_agent.o_resp_channel.deq();
       injC <= True;
       wr_flitinj <= flit;
     end
		endrule

    rule rl_inject_flit(injA || injB || injC);
      let flit = wr_flitinj;
      flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType );
      mesh.putFlit(flit);
    endrule

		rule rl_transfer_m_SysMon(rules_can_fire);
			Sys_mon_type#(a,w) lv_flit_data = fifo_m_sys_mon.first();
			Bit#(DataSz) data = zeroExtend(pack(lv_flit_data));
			// dest to coords
			LookAheadRouteInfo routeInfo = fn_encode_routing_info(Len {	x_len:fromInteger(node_id_x) ,y_len:fromInteger(node_id_y) },
																																	lv_flit_data.node);
      //let lv_crit = lv_flit_data.user;
			Flit flit = Flit { msgType : pack(SysManagement),
													critType : crit,
													vc : ?,
													flitType : HeadTail,
													routeInfo : routeInfo,
													flitData : data};
			flit.vc <- vcAllocUnit.getNextVC(flit.msgType,flit.critType );
			mesh.putFlit(flit);
			//$display("S S\t\t",fshow(flit));
			fifo_m_sys_mon.deq();
		endrule

		rule rl_buffer_incoming_flits;
			Flit flit <- mesh.getFlit();
			//creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
			f_recieve_flit[flit.msgType].enq(flit);
      //buf_msgType.enq(flit.msgType);
		endrule

		//rule rl_transfer_s_A(rules_can_fire && (buf_msgType.first == 0));
		rule rl_transfer_s_A(rules_can_fire );
			Flit flit = f_recieve_flit[pack(ShaktiLink_Req_channel)].first();
			Req_channel#(a,w,o,i,op,u) lv_flit_data = unpack(truncate(flit.flitData));
			sl_master_agent.i_req_channel.enq(lv_flit_data);
			f_recieve_flit[pack(ShaktiLink_Req_channel)].deq();
			//creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
      buf_flit.enq(flit);
      wrA <= True;
		endrule

		//rule rl_transfer_m_B(rules_can_fire && (buf_msgType.first == 1));
		rule rl_transfer_m_B(rules_can_fire );
			Flit flit = f_recieve_flit[pack(ShaktiLink_Fwd_channel)].first();
			//creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
			Fwd_channel#(a,w,o,i,op,u) lv_flit_data = unpack(truncate(flit.flitData));
			sl_slave_agent.i_fwd_channel.enq(lv_flit_data);
			f_recieve_flit[pack(ShaktiLink_Fwd_channel)].deq();
      buf_flit.enq(flit);
      wrB <= True;
		endrule

		//rule rl_transfer_m_D(rules_can_fire && (buf_msgType.first == 2));
		rule rl_transfer_m_D(rules_can_fire );
			Flit flit = f_recieve_flit[pack(ShaktiLink_Resp_channel)].first();
			//creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
			Resp_channel#(a,w,o,i,op,acks,u) lv_flit_data = unpack(truncate(flit.flitData));
			sl_slave_agent.i_resp_channel.enq(lv_flit_data);
			f_recieve_flit[pack(ShaktiLink_Resp_channel)].deq();
      buf_flit.enq(flit);
      wrC <= True;
		endrule

    rule rl_A_B_C_fired(wrA || wrB || wrC);
      let flit = buf_flit.first;
      creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
      buf_flit.deq;
      //buf_msgType.deq;
    endrule


		rule rl_transfer_s_SysMon(rules_can_fire);
			Flit flit = f_recieve_flit[pack(SysManagement)].first();
			creditUnit.putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType, critType: flit.critType}));
			Sys_mon_type#(a,w) lv_flit_data = unpack(truncate(flit.flitData));
			fifo_s_sys_mon.enq(lv_flit_data);
			f_recieve_flit[pack(SysManagement)].deq();
		endrule

		interface node_master = sl_master_agent.shaktilink_side;
		interface node_slave  = sl_slave_agent.shaktilink_side;
		interface sysmon_out = toGet(fifo_m_sys_mon);
		interface sysmon_in = toPut(fifo_s_sys_mon);

		method Action injection_rate_governor(Bool arg_rules_can_fire);
			rules_can_fire <= arg_rules_can_fire;
		endmethod
		method Action local_criticality (Bool criticality);
			if (criticality)
				crit <= 1'b1;
		endmethod

	endmodule
endpackage








