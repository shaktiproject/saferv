package address_map;

import ShaktiLink_Types::*;
import RoutingTypes::*;
import Types::*;

`include "system.defines"

function Bit#(o) fn_addr_to_id(Bit#(a) addr);
	if(addr >= `UartBase && addr <= `UartEnd)
		return 9;
	else if(addr >= 'h8000_0000 && addr <= 'h8000_FFFF)
		return 4;
	else if(addr >= 'h8001_0000 && addr <= 'h8001_FFFF)
		return 5;
	else if(addr >= 'h8002_0000 && addr <= 'h8002_FFFF)
		return 6;
	else 
		return 7;
endfunction

// function Bit#(i) address_map (Bit#(a) address);
// 	Bit#(i) lv_destination = 0;
// 	if     ( (address >= `Node0_Base) && (address <= `Node0_End)) lv_destination = 0;
// 	else if( (address >= `Node1_Base) && (address <= `Node1_End)) lv_destination = 1;
// 	else if( (address >= `Node2_Base) && (address <= `Node2_End)) lv_destination = 2;
// 	else if( (address >= `Node3_Base) && (address <= `Node3_End)) lv_destination = 3;
// 	else if( (address >= `Node4_Base) && (address <= `Node4_End)) lv_destination = 4;
// 	else if( (address >= `Node5_Base) && (address <= `Node5_End)) lv_destination = 5;
// 	else if( (address >= `Node6_Base) && (address <= `Node6_End)) lv_destination = 6;
// 	else if( (address >= `Node7_Base) && (address <= `Node7_End)) lv_destination = 7;
// 	else if( (address >= `Node8_Base) && (address <= `Node8_End)) lv_destination = 8;
// 	else if( (address >= `Node9_Base) && (address <= `Node9_End)) lv_destination = 9;
// 	else if( (address >= `NodeA_Base) && (address <= `NodeA_End)) lv_destination = 10;
// 	else if( (address >= `NodeB_Base) && (address <= `NodeB_End)) lv_destination = 11;
// 	else if( (address >= `NodeC_Base) && (address <= `NodeC_End)) lv_destination = 12;
// 	else if( (address >= `NodeD_Base) && (address <= `NodeD_End)) lv_destination = 13;
// 	else if( (address >= `NodeE_Base) && (address <= `NodeE_End)) lv_destination = 14;
// 	else if( (address >= `NodeF_Base) && (address <= `NodeF_End)) lv_destination = 15;
// 	return lv_destination;
// endfunction

function Bit#(o) req_move_from_mi_to_sj (Bit#(a) addr,Bit#(o) dest) provisos(Add#(1, a__, o));
	Bit#(1) filter = truncateLSB(dest);
	let slave_num = fn_addr_to_id(addr);
	if (filter == 0)
		slave_num = truncate(dest);
	return slave_num;
endfunction

function Bit#(i) fwd_move_from_sj_to_mi (Bit#(a) addr,Bit#(i) dest)provisos(Add#(1, a__, i));
	Bit#(1) filter = truncateLSB(dest);
	let slave_num = fn_addr_to_id(addr);
	if (filter == 0)
		slave_num = truncate(dest);
		return slave_num;
endfunction

function Bit#(i) resp_move_from_sj_to_mi (Bit#(a) addr,Bit#(i) dest)provisos(Add#(1, a__, i));
	Bit#(1) filter = truncateLSB(dest);
	let slave_num = fn_addr_to_id(addr);
	if (filter == 0)
		slave_num = truncate(dest);
	return slave_num;
endfunction


// Map TL source sink to Coordinates 
function Len fn_master_to_coordinates(Bit#(o) master_id);
	case (master_id) matches
		0:  return Len {y_len:0 ,x_len:0 };
		1:  return Len {y_len:0 ,x_len:1 };
		2:  return Len {y_len:0 ,x_len:2 };
		3:  return Len {y_len:0 ,x_len:3 };
		4:  return Len {y_len:1 ,x_len:0 };
		5:  return Len {y_len:1 ,x_len:1 };
		6:  return Len {y_len:1 ,x_len:2 };
		7:  return Len {y_len:1 ,x_len:3 };
		8:  return Len {y_len:2 ,x_len:0 };
		9:  return Len {y_len:2 ,x_len:1 };
		10: return Len {y_len:2 ,x_len:2 };
		11: return Len {y_len:2 ,x_len:3 };
		12: return Len {y_len:3 ,x_len:0 };
		13: return Len {y_len:3 ,x_len:1 };
		14: return Len {y_len:3 ,x_len:2 };
		15: return Len {y_len:3 ,x_len:3 };
	endcase
endfunction
function Len fn_slave_to_coordinates(Bit#(i) slave_id);
	case (slave_id) matches
		0:  return Len {y_len:0 ,x_len:0 };
		1:  return Len {y_len:0 ,x_len:1 };
		2:  return Len {y_len:0 ,x_len:2 };
		3:  return Len {y_len:0 ,x_len:3 };
		4:  return Len {y_len:1 ,x_len:0 };
		5:  return Len {y_len:1 ,x_len:1 };
		6:  return Len {y_len:1 ,x_len:2 };
		7:  return Len {y_len:1 ,x_len:3 };
		8:  return Len {y_len:2 ,x_len:0 };
		9:  return Len {y_len:2 ,x_len:1 };
		10: return Len {y_len:2 ,x_len:2 };
		11: return Len {y_len:2 ,x_len:3 };
		12: return Len {y_len:3 ,x_len:0 };
		13: return Len {y_len:3 ,x_len:1 };
		14: return Len {y_len:3 ,x_len:2 };
		15: return Len {y_len:3 ,x_len:3 };
	endcase
endfunction

function LookAheadRouteInfo fn_encode_routing_info (Len source,Len sink); 
	LookAheadRouteInfo routeInfo = unpack(0);

	routeInfo.dirX = (sink.x_len > source.x_len )? WE_:EW_;
  routeInfo.numXhops = (sink.x_len  > source.x_len )? (sink.x_len -source.x_len ) : (source.x_len -sink.x_len );	
	routeInfo.dirY = (sink.y_len >source.y_len )? NS_:SN_; 
	routeInfo.numYhops = (sink.y_len  > source.y_len )? (sink.y_len -source.y_len ) : (source.y_len -sink.y_len );

  if(sink.x_len  != source.x_len ) begin// Initial direction: X                                                 
    routeInfo.nextDir = (sink.x_len  > source.x_len )? east_ : west_;                                      
  end                                                                                           
  else if(sink.y_len  !=source.y_len ) begin //Initial direction: Y                                             
    routeInfo.nextDir = (sink.y_len  > source.y_len )? south_:north_;                                      
  end                                                                                           
  else begin                                                                                    
    routeInfo.nextDir = local_;
	end
	return routeInfo;
endfunction
endpackage
