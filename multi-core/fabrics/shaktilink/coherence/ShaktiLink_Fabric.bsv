/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package ShaktiLink_Fabric;
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import Vector    :: *;
  import ShaktiLink_Types :: *;
  import Semi_FIFOF :: * ;

  interface Ifc_ShaktiLink_Fabric#(numeric type num_masters, numeric type num_slaves, 
                            numeric type a, numeric type w, numeric type o, 
                            numeric type i, numeric type op, numeric type acks, numeric type u);
                            
    interface Vector#(num_masters, Ifc_slc_slave#(a,w,o,i,op,acks,u)) v_from_masters;
    interface Vector#(num_slaves , Ifc_slc_master#(a,w,o,i,op,acks,u)) v_to_slaves;
  endinterface


  module mkShaktiLink_Fabric#(function Bit#(TLog #(num_slaves)) fn_addr_to_id
          (Bit #(a) addr))(Ifc_ShaktiLink_Fabric#(num_masters,num_slaves,a,w,o,i,op,acks,u))
    provisos (Log #(num_masters, log_nm),
	     Log #(num_slaves,  log_ns),
	     Log #(TAdd #(num_masters, 1), log_nm_plus_1),
	     Log #(TAdd #(num_slaves,  1), log_ns_plus_1),
	     Add #(_dummy, TLog #(num_slaves), log_ns_plus_1),
	     Add#(a__, TLog#(num_slaves), i),
	     // required by BSC
	     Add#(1, b__, i)
	   );
    // Transactors facing masters
    Vector #(num_masters, Ifc_slc_slave_agent#(a,w,o,i,op,acks,u))
       xactors_from_masters <- replicateM (mkslc_slave_agent);

    // Transactors facing slaves
    Vector #(num_slaves,  Ifc_slc_master_agent#(a,w,o,i,op,acks,u))
        xactors_to_slaves    <- replicateM (mkslc_master_agent);
    
    
    function Bool req_move_from_mi_to_sj (Integer mi, Integer sj);
      let addr = xactors_from_masters [mi].o_req_channel.first.address;
      let dest = xactors_from_masters [mi].o_req_channel.first.dest;
      Bit#(1) filter = truncateLSB(dest);
      let slave_num = fn_addr_to_id(addr);
      if (filter == 0)
        slave_num = truncate(dest);
      return (slave_num == fromInteger (sj));
    endfunction
    
    function Bool fwd_move_from_sj_to_mi (Integer sj, Integer mi);
      let addr = xactors_to_slaves [sj].o_fwd_channel.first.address;
      let dest = xactors_to_slaves [sj].o_fwd_channel.first.dest;
      Bit#(1) filter = truncateLSB(dest);
      let slave_num = fn_addr_to_id(addr);
      if (filter == 0)
        slave_num = truncate(dest);
      return (slave_num == fromInteger (mi));
    endfunction
    function Bool resp_move_from_sj_to_mi (Integer sj, Integer mi);
      let addr = xactors_to_slaves [sj].o_resp_channel.first.address;
      let dest = xactors_to_slaves [sj].o_resp_channel.first.dest;
      Bit#(1) filter = truncateLSB(dest);
      let slave_num = fn_addr_to_id(addr);
      if (filter == 0)
        slave_num = truncate(dest);
      return (slave_num == fromInteger (mi));
    endfunction
   
  for (Integer mi = 0; mi < valueOf (num_masters); mi = mi + 1)
    for (Integer sj = 0; sj < valueOf (num_slaves); sj = sj + 1)

	 rule rl_req_master_to_slave (req_move_from_mi_to_sj (mi, sj));
	    Req_channel#(a,w,o,i,op,u) a <- pop_o (xactors_from_masters [mi].o_req_channel);

	    xactors_to_slaves [sj].i_req_channel.enq (a);

	    `logLevel( fabric, 0, $format("SLC_Fabric: REQ master [%0d] -> slave [%0d]", mi, sj))
	    `logLevel( fabric, 0, $format("", fshow (a)))
	 endrule
	 
    for (Integer sj = 0; sj < valueOf (num_slaves); sj = sj + 1)
      for (Integer mi = 0; mi < valueOf (num_masters); mi = mi + 1)
	      rule rl_fwd_slave_to_master (fwd_move_from_sj_to_mi (sj, mi));
	         Fwd_channel#(a,w,o,i,op,u) a <- pop_o (xactors_to_slaves [sj].o_fwd_channel);

	         xactors_from_masters[mi].i_fwd_channel.enq (a);


	         `logLevel( fabric, 0, $format("SLC_Fabric: FWD slave [%0d] -> master [%0d]", sj, mi))
	         `logLevel( fabric, 0, $format("", fshow (a)))
	      endrule
    
    for (Integer sj = 0; sj < valueOf (num_slaves); sj = sj + 1)
      for (Integer mi = 0; mi < valueOf (num_masters); mi = mi + 1)
	      rule rl_resp_slave_to_master (resp_move_from_sj_to_mi (sj, mi));
	         Resp_channel#(a,w,o,i,op,acks,u) a <- pop_o (xactors_to_slaves [sj].o_resp_channel);

	         xactors_from_masters[mi].i_resp_channel.enq (a);


	         `logLevel( fabric, 0, $format("SLC_Fabric: RESP slave [%0d] -> master [%0d]", sj, mi))
	         `logLevel( fabric, 0, $format("", fshow (a)))
	      endrule
   
   function Ifc_slc_slave#(a,w,o,i,op,acks,u) f1 (Integer j) = xactors_from_masters[j].shaktilink_side;
   function Ifc_slc_master#(a,w,o,i,op,acks,u) f2 (Integer j) = xactors_to_slaves[j].shaktilink_side;
   interface v_from_masters = genWith (f1);
   interface v_to_slaves    = genWith (f2);
 endmodule
endpackage

