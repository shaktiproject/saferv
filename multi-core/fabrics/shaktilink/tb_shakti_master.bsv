package tb_shakti_master;

  import StmtFSM :: * ;
  import GetPut::*;
  import FIFO::*;
  import FIFOF::*;
	import SpecialFIFOs :: * ;
	import Semi_FIFOF::*;
  import LFSR::*;
	import ShaktiLink_Types::*;
	import Tilelink_Types::*;
  import Vector::*; //for rotateBitsBy

  interface Ifc_sl_master_tg#(numeric type a, numeric type w, numeric type o,
	numeric type i, numeric type op, numeric type acks, numeric type u);
    method Action ma_start;// used to start the TG
    method Action ma_finish;// used to stop the TG
		method Action ma_rand_value(Bit#(a) lfsr_value);
		interface Ifc_slc_master#(a,w,o,i,op,acks,u) sl_master;
    method Bool mv_done;
		method Bit#(32) get_recv_count;
  endinterface

    //module parameter address map
  (*synthesize*)
  module mk_instance_sl_master_tg(Empty);
    Ifc_sl_master_tg#(32, 64,2,2,1,1,1) instant <- mk_sl_master_tg('h0);
  endmodule

  module mk_sl_master_tg#(Integer source_id)(Ifc_sl_master_tg#(a,w,o,i,op,acks,u))
	provisos(Add#(a__, op, 3),Add#(b__, 3, a));

    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) rg_finish <- mkReg(False);
		Reg#(Bit#(32))       req_count      <- mkReg(0);
		// Master Agent
		Ifc_slc_master_agent#(a,w,o,i,op,acks,u) sl_master_agent <- mkslc_master_agent();

    // Generate a Transaction Signature
    LFSR#(Bit#(4))  rand_event <- mkLFSR_4();
    // How to generate an address Generate any address , if it corresponds to the local id of the setup then
    FIFO#(Bit#(a)) fifo_txn_signature <- mkSizedFIFO(2);

    //Reg#(Bit#(z)) rg_beat_counter <- mkReg(0);
    Reg#(Bit#(a)) rg_acquire_address<- mkReg(unpack(0));
		Reg#(Bit#(3)) rg_acquire_size   <- mkReg(unpack(0));
		Reg#(Bit#(i))	rg_grant_ack_dest <- mkReg(unpack(0));
    Reg#(Bit#(a)) rg_release_address<- mkReg(unpack(0));
    Reg#(Bit#(3)) rg_release_size   <- mkReg(unpack(0));
		Reg#(Bool)    rg_wait_for_release_ack <- mkReg(False);

		// Start Sequence

    Reg#(Bit#(u)) rg_crit <- mkReg(1);
    Reg#(Bit#(4))  rg_count       <- mkReg(0);
    Reg#(Bool) rg_acquire_block <- mkReg(False);
		Reg#(Bool) rg_start_a <- mkReg(False);
		Reg#(Bool) rg_start_d <- mkReg(False);
		Reg#(Bool) rg_start_r <- mkReg(False);
		Reg#(Bool) rg_start_rd <- mkReg(False);
		Reg#(Bool) rg_start_b <- mkReg(False);
		Reg#(Bool) rg_wait_release_ack <- mkReg(False);

		// rule rl_display;
		// 	$display("SOURCE:: %x ::  %x,%x,%x,%x,%x,%x,%x",fromInteger(source_id),rg_acquire_block,rg_start_a,rg_start_d,rg_start_r,rg_start_rd,rg_start_e,rg_wait_release_ack);
		// endrule

    // make a request packet
    // size and destination address are random
    // put the packet into the master
		rule rl_drive_A (rg_start_a && (!rg_finish));
			let txn = fifo_txn_signature.first();
			Req_channel#(a,w,o,i,op,u) packet_acquire = ?;
			Bit#(op) lv_op = truncate(pack(AcquireBlock));
			//packet_acquire.opcode = unpack(lv_op);
			// for testing purposes, opcode caries the crit of packet
      rg_crit <= rg_crit + 1 ;
      Bit#(u) lv_crit = 0;
      if(rg_count%8==0) begin
        lv_crit = 0;
      end
      else begin
        lv_crit = 1;
      end

      rg_count <= rg_count +1;
      packet_acquire.user = unpack(lv_crit);
			packet_acquire.size = 1;
			//packet_acquire.size = truncate(txn);
			//packet_acquire.crit = truncate(txn);
      packet_acquire.address = txn;
			packet_acquire.source = fromInteger(source_id);
			fifo_txn_signature.deq();
			//counting the total req injection for each node
			req_count <= req_count + 1;
			sl_master_agent.i_req_channel.enq(packet_acquire);
			$display($time,"Req gen: %d",source_id,fshow(packet_acquire));
			rg_acquire_size <= 1;
			//rg_acquire_size <= truncate(txn);
			rg_start_a <= False;
			//rg_start_b <= True;
			rg_start_d <= True;
		endrule

    // Concurrent B - Channel handling
		// B- Channel fwd response
		// B- Channel fwd-grant   response
		rule rl_drive_B(rg_start_b );
			Fwd_channel#(a,w,o,i,op,u) grant_packet = sl_master_agent.o_fwd_channel.first();
			Bit#(op) lv_op_grant = pack(grant_packet.opcode);
			if((lv_op_grant == truncate(pack(GrantData))) ||(lv_op_grant == truncate(pack(Grant))))begin
				$display($time, "Fwd recv: %d",source_id,fshow(grant_packet));
				sl_master_agent.o_fwd_channel.deq();
				rg_grant_ack_dest <= grant_packet.dest;
				if(rg_acquire_size > 0 )
					rg_acquire_size <= rg_acquire_size - 1;
				else begin
					rg_start_b <= False;
          rg_acquire_block <= False;
				end
			end
			else if(lv_op_grant == truncate(pack(ReleaseAck)))begin

			end
		endrule


		// Concurrent D - Channel handling
		// D- Channel release response
		// D- Channel grant response
		rule rl_drive_D(rg_start_d || rg_wait_release_ack );
			Resp_channel#(a,w,o,i,op,acks,u) grant_packet = sl_master_agent.o_resp_channel.first();
			Bit#(op) lv_op_grant = pack(grant_packet.opcode);
			if((lv_op_grant == truncate(pack(GrantData))) ||(lv_op_grant == truncate(pack(Grant))))begin
				$display($time,"Response recv: %d",source_id,fshow(grant_packet));
				sl_master_agent.o_resp_channel.deq();
				rg_grant_ack_dest <= grant_packet.dest;
				if(rg_acquire_size > 0 )
					rg_acquire_size <= rg_acquire_size - 1;
				else begin
					rg_start_d <= False;
          rg_acquire_block <= False;
				end
			end
			else if(lv_op_grant == truncate(pack(ReleaseAck)))begin

			end
		endrule
		//tl_master_agent.o_b_channel

		    // Start
    Reg#(Bit#(4)) rg_event <- mkReg(0);


    rule rl_reset_acquire_block ( ! rg_acquire_block && rg_start);
			rg_start_a <= True;
			rg_start_b <= False;
			rg_start_d <= False;
      rg_acquire_block <= True;
    endrule

  // Interfaces
		 method Bit#(32) get_recv_count;
      return req_count;
    endmethod


    method Action ma_start if(!rg_start);
      rg_start <= True;
      rand_event.seed(4'ha);
    endmethod
    method Action ma_finish if (rg_start);
      rg_finish <= True;
		endmethod
		method Action ma_rand_value(Bit#(a) lfsr_value) if(rg_start);
			Bit#(o) rot_val = fromInteger(source_id);
      fifo_txn_signature.enq(rotateBitsBy(lfsr_value,unpack(fromInteger(source_id))));
    endmethod
    method Bool mv_done if (rg_finish);
      return True;
    endmethod

    interface sl_master = sl_master_agent.shaktilink_side;
  endmodule
endpackage :tb_shakti_master
