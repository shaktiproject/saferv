/*
 * see LICENSE.iitm
 *
 * Author:
 * Email id:
 *
 * Details: Global include file for both synthesis & simulation in Vivado
 */

`ifdef BSV_RESET_FIFO_HEAD
`else
  `define BSV_RESET_FIFO_HEAD
`endif

`ifdef BSV_RESET_FIFO_ARRAY
`else
  `define BSV_RESET_FIFO_ARRAY
`endif
