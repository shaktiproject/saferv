#-------------- MCS Generation ----------------------
set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN div-1  [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES       [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 8          [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE          [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN Pulldown      [current_design]
set_property CFGBVS GND                               [current_design]
set_property CONFIG_VOLTAGE 1.8                       [current_design]
set_property CONFIG_MODE SPIx8                        [current_design]

#-------------- Port-PIN mapping ----------------------

set_property PACKAGE_PIN E12 [get_ports c0_sys_clk_p]
set_property IOSTANDARD DIFF_SSTL12 [get_ports c0_sys_clk_p]

set_property PACKAGE_PIN D12 [get_ports c0_sys_clk_n]
set_property IOSTANDARD DIFF_SSTL12 [get_ports c0_sys_clk_n]

create_clock -period 4.000 [get_ports c0_sys_clk_p]
set_input_jitter [get_clocks -of_objects [get_ports c0_sys_clk_p]] 0.05

# CPU_RESET
set_property -dict { PACKAGE_PIN L19 IOSTANDARD LVCMOS12 } [get_ports {sys_rst}];

# UART ports
set_property IOB TRUE [get_ports uart_SOUT]
set_property IOB TRUE [get_ports uart_SIN]

set_property -dict {PACKAGE_PIN BB21 IOSTANDARD LVCMOS18 } [get_ports {uart_SOUT}];
set_property -dict {PACKAGE_PIN AW25 IOSTANDARD LVCMOS18 } [get_ports {uart_SIN}];
