import Fifo::*;
import Types::*;
import VirtualChannelTypes::*;
import CreditTypes::*;
import SpecialFIFOs ::*;
import MessageTypes::*;
import Vector::*;
import FIFOF::*;

interface ReverseCreditUnit;
  /* Credit */
  method Action putCredit(CreditSignal credit);
  `ifdef SMART
  method Action putCreditSMART(CreditSignal credit);
  `endif
  method ActionValue#(CreditSignal) getCredit;
endinterface

// TODO
// 2 explore:
// mkPipelineFifo with depth 1
// normal FIFO with depth 2
(* synthesize *)
module mkReverseCreditUnit(ReverseCreditUnit);
//  Fifo#(NumVCs, CreditSignal)     creditQueue <- mkPipelineFifo;
//	FIFOF#(CreditSignal) creditQueue <- mkPipelineFIFOF;

/// NumVNETs is the total number of VNET's, like 3 VNETs
/// creditQueue[--index-crit--][--index-vnet--]
  Vector#(CritLevels,Vector#(NumVNETs,FIFOF#(CreditSignal))) creditQueue <- replicateM(replicateM(mkSizedFIFOF(2)));
  Fifo#(NumVCs, CreditSignal)     smartCreditQueue <- mkPipelineFifo;
  Reg#(MsgType) msgType <- mkReg(0); // arbitrary initializing with Data message type
  Reg#(CritType) critType <- mkReg(0); //arbitrary initializing with HI message type

//Rule to check the content of creditQueue over time.
// Due to aggressive conditions during compile time, this rule will unroll into
// separate (CritLevels * NumVNETs) independent rules,
//for(Integer i = 0; i < 3; i = i+1) begin
  rule rl_display_cq;
    for(Integer crit=0; crit < fromInteger(valueOf(CritLevels)); crit = crit +1)begin
      for(Integer vnet = 0; vnet < fromInteger(valueOf(NumVNETs)); vnet = vnet+1) begin
        if(creditQueue[crit][vnet].notEmpty)begin
          let lv_credit = creditQueue[crit][vnet].first();
          //
          //$display($time, "\t credit queue content for crit: %d, VNET: %d is :", crit,vnet, fshow(lv_credit));
        end
      end
    end
  endrule
//end


  method Action putCredit(CreditSignal credit);
    if(isValid(credit)) begin
      ///enqueue into correct creditQueue based on message class and criticality

      let lv_vnet = validValue(credit).msgType;
      let lv_crit = validValue(credit).critType;
      creditQueue[lv_crit][lv_vnet].enq(credit);
      msgType <= lv_vnet;
      critType <= lv_crit;

      //$display($time, "\tputCredit happening!, for credit : ", fshow(credit));

    end
  endmethod




`ifdef SMART
  method Action putCreditSMART(CreditSignal credit);
    if(isValid(credit)) begin
      smartCreditQueue.enq(credit);
    end
  endmethod
`endif

// Cyele PATH breaks.. !
// creditQueue is a pipeline FIFO.
// putCredit and getCredit can't happen in the same cycle for a empty creditQueue.
// essentially, the number of entries used in creditQueue are either 0 or 1.
// later the enq and deq happens together but for different flits.
  method ActionValue#(CreditSignal) getCredit;
    CreditSignal credit = Invalid;

    let lv_vnet = msgType;
    let lv_crit = critType;
    if(creditQueue[lv_crit][lv_vnet].notEmpty) begin
      creditQueue[lv_crit][lv_vnet].deq;
      credit = creditQueue[lv_crit][lv_vnet].first();
    end
    //else if(smartCreditQueue.notEmpty) begin
    //  smartCreditQueue.deq;
    //  credit = smartCreditQueue.first();
    //end
    //else begin
    //  credit = Invalid;
    //end

    //$display($time, "\tgetCredit is happening!, for credit: ", fshow(credit));
    return credit;
  endmethod

endmodule
