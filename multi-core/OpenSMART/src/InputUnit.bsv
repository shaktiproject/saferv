import Vector::*;
import Fifo::*;
import CReg::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;

import NtkArbiter::*;

interface InputUnit;
  method Bool           isInited;

  /* Flit */
  method Action         putFlit(Flit flit);
  method Maybe#(Flit)   peekFlit;
  method Action         deqFlit;

  /* Header Information */
  method Maybe#(Header) peekHeader;
endinterface

(* synthesize *)
module mkInputUnit(InputUnit);
  /********************************* States *************************************/
  Reg#(Bool)              inited <- mkReg(False);
  CReg#(4, Maybe#(VCIdx)) nextVC <- mkCReg(Invalid);

  /******************************* Submodules ***********************************/

  // due to mkConnection, flit can't sit directly into the fifo. Need to receive
  // the flit in a register and then decode before enqueueing into fifo.
  CReg#(3, Maybe#(Flit))                         arrivingFlit <- mkCReg(Invalid);

  Vector#(NumVCs, CReg#(3, Data)) numFlits <- replicateM(mkCReg(0));

  Vector#(NumVCs, Fifo#(MaxVCDepth, Flit))   vcs       <- replicateM(mkBypassFifo);
  Vector#(NumVCs, Fifo#(1, Header))          headers   <- replicateM(mkBypassFifo);
  NtkArbiter#(NumVCs)                        vcArbiter <- mkInputVCArbiter;  //Arbiter

  /******************************* Functions ***********************************/
  function Bit#(NumVCs) getArbitReqBits;
  //Generate an arbitration request bit by investigating the VC queues
    // one hot encoding
    Bit#(NumVCs) reqBit = 0;

    for(VCIdx vc=0; vc<fromInteger(valueOf(NumVCs)); vc=vc+1)
    begin
      // set req bit to 0 if the VC doesn't have any flits
      reqBit[vc] = (numFlits[vc][2] == 0)? 0:1;
    end

    if(isValid(arrivingFlit[2])) begin
      let newFlit = validValue(arrivingFlit[2]);
      reqBit[newFlit.vc] = 1;
    end

    return reqBit;
  endfunction

  // Dummy function: doesn't get used
  function ActionValue#(Maybe#(VCIdx)) getNextVC;
  actionvalue
    Bit#(NumVCs) reqBit = getArbitReqBits();
     let arbitRes <- vcArbiter.getArbit(reqBit);

    let winnerIdx = arbitRes2Idx(arbitRes);  //winner index is Invalid if there is no winner.

    return winnerIdx;
  endactionvalue
  endfunction

// input port arbitration is happening in the cycle where link traversal, input buffer write
// is happening. The arbitration is performed on the incoming flit(vc) (like on arriving_flit)
// and the winner of arbitration is reflected in the next cycle.
  function Action doArbit;
  action
    // find all the participating input VC ID's
    Bit#(NumVCs)  reqBit = getArbitReqBits();

    //$display($time, "\t Original ReqBit: %b",reqBit);

		Bit#(1) high_part = 0;
    //find if there is any high critical VC participating
    for(Integer vc=0; vc < fromInteger(valueOf(NumVCs)); vc = vc+1)begin
      let crit = vc%2;
      if(crit==0 && reqBit[vc]==1)begin
        high_part = 1;
        end
    end

    //$display($time, "\t Initial ReqBit: %d", reqBit);
    // if there are any HI participating, make reqBit of LO VCs zero
    for(Integer vc =0; vc < fromInteger(valueOf(NumVCs)); vc = vc+1)begin
      //let vnet = vc/fromInteger(valueOf(VCperVNET));
      let crit = vc%2;
      if(high_part==1 && (vc%2)==1)begin
        reqBit[vc]=0;
      end
    end


     //$display($time, "\t high_part==%d, ReqBit: %b", high_part,reqBit);

    let arbitRes <- vcArbiter.getArbit(reqBit);
`ifdef DEBUG
    if(reqBit != 0)
      $display("Arbit Request: %b, res: %b",reqBit,arbitRes);
`endif

    let winnerIdx = arbitRes2Idx(arbitRes);  //winner index is Invalid if there is no winner.
    nextVC[3] <= winnerIdx;
  endaction
  endfunction


  /**************************** InputUnit Behavior *****************************/
  rule doInitialize(!inited);
    inited <= True;
    vcArbiter.initialize;
  endrule

  // load the flit and header into buffers
  rule rl_loadFlit(inited && isValid(arrivingFlit[0]));
    /*
    let flit = arrivingFlit.first;
    arrivingFlit.deq;
    */

    let flit = validValue(arrivingFlit[0]);
    let vc = flit.vc;
    vcs[vc].enq(flit);
    let critType = flit.critType;

    if(isHead(flit)) begin
      headers[vc].enq(Header{vc: vc, routeInfo: flit.routeInfo, msgType: flit.msgType, critType: critType});
    end
    // writing Invalid at 0th port to give more priority to any other Valid value
    arrivingFlit[0] <= Invalid;
  endrule

  rule rl_doArbit(inited);// && !isValid(nextVC[2]));
    doArbit;
  endrule

  /******************************* Interfaces **********************************/
  method Bool isInited = inited;

  method Maybe#(Header) peekHeader if(inited);
    if(isValid(nextVC[1])) begin
      let vc = validValue(nextVC[1]);
      return Valid(headers[vc].first);
    end
    else begin
        return Invalid;
    end
  endmethod

  method Action putFlit(Flit flit) if(inited);
    numFlits[flit.vc][0] <= numFlits[flit.vc][0] + 1;
    arrivingFlit[1] <= Valid(flit);
//    arrivingFlit.enq(flit);
  endmethod

  method Maybe#(Flit) peekFlit if(inited);
    if(isValid(nextVC[1])) begin
      let vc = validValue(nextVC[1]);
      let topFlit = vcs[vc].first;
      return Valid(topFlit);
    end
    else begin
        return Invalid;
    end
  endmethod

  method Action deqFlit if(inited && isValid(nextVC[1]));
      if(isValid(nextVC[1])) begin
        let vc = validValue(nextVC[1]);
        vcs[vc].deq;
        // numFlits is written at lower priority port as\
        // getArbitReq is dependent on it.
        // For ex : in a cycle, the input port arbitration and OParb will
        // happen together. But, winner of one input port arb is
        // having OParb in the next cycle.
        // So, in a cycle, the input port arb should happen on the flits
        // including the one's who have lost/won output port arb.
        numFlits[vc][1] <= numFlits[vc][1]-1;
        if(isTail(vcs[vc].first)) begin
          headers[vc].deq;
      //  if(isValid(nextVC[2]) && (validValue(nextVC[2]) == vc))
//          nextVC[2] <= Invalid; //multi-flit
        end
      end
  endmethod
endmodule
