/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package imem_controller;
  `include "Logger.bsv"
  `include "coherence.defines"
  `include "common_tlb.defines"
  import coherence_types :: * ;
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import GetPut :: * ;
  import Vector :: * ;
  import Semi_FIFOF :: * ;
  import ShaktiLink_Types :: * ;
  import imem :: *;
  import icache_types :: * ;
  import common_tlb_types :: * ;
  import globals :: * ;

//  `define O TAdd#(TLog#(`NrCaches),1)
//  `define I TAdd#(TLog#(`NrCaches),1)

  interface Ifc_imem_controller#(numeric type o, numeric type i);
    interface Ifc_slc_master#(`paddr, TMul#(`dwords,`dblocks),o,i,SizeOf#(MessageType),
                            TLog#(`NrCaches), 1) master_side;

      // -------------------- Cache related interfaces ------------//
    interface Put#(IMem_core_request#(`vaddr, `iesize )) core_req;
    interface Get#(IMem_core_response#(TMul#(`iwords, 8), `iesize )) core_resp;
    method Action cache_enable(Bool c);
    method Bool cache_available;
    method Action ma_criticality(Bit#(1) c);
      // ---------------------------------------------------------//
      // - ---------------- TLB interfaces ---------------------- //
  `ifdef supervisor
    interface Get#(PTWalk_tlb_request#(`vaddr)) req_to_ptw;
    interface Put#(PTWalk_tlb_response#(TAdd#(`ppnsize,10), `varpages)) response_frm_ptw;
    /*doc:method: method to receive the current satp csr from the core*/
    method Action ma_satp_from_csr (Bit#(`vaddr) s);

    /*doc:method: method to recieve the current privilege mode of operation*/
    method Action ma_curr_priv (Bit#(2) c);
    `ifdef pmp
      /*doc:method: */
      method Action ma_pmp_cfg ( Vector#(`PMPSIZE, Bit#(8)) pmpcfg) ;
      /*doc:method: */
      method Action ma_pmp_addr ( Vector#(`PMPSIZE, Bit#(`paddr)) pmpaddr);
    `endif
  `endif

`ifdef perfmonitors
  `ifdef icache
    method Bit#(5) mv_icache_perf_counters;
  `endif
  `ifdef supervisor
    method Bit#(1) mv_itlb_perf_counters ;
  `endif
`endif
  endinterface

  function Req_channel#(a,w,o,i,op,u) fn_gen_req_pkt(Message#(a,w) m, Bit#(o) id, Bit#(u) user)
    provisos(Add#(a__, 2, i),Add#(b__, 4, op));
    let _r = Req_channel{ opcode: zeroExtend(pack(m.msgtype)),
                          len: 0,
                          size: 3,		                       
                          mode:0,
                          source: zeroExtend(pack(id)), 
                          dest: signExtend(pack(m.dst)), 
                          address:m.address, 
                          mask:'1,
                          data:m.cl,
                          user: user}; 
    return _r;
  endfunction
  
  function Fwd_channel#(a,w,o,i,op,u) fn_gen_fwd_pkt(Message#(a,w) m, Bit#(u) user)
    provisos(Add#(a__, 2, i),Add#(b__, 2, o),Add#(c__, 4, op));
    let _r = Fwd_channel{ opcode: zeroExtend(pack(m.msgtype)),
                          source: zeroExtend(pack(m.src)), 
                          dest:signExtend(pack(m.dst)), 
                          address:m.address, 
                          mask:'1,
                          data:m.cl,
                          user: user}; 
    return _r;
  endfunction
  
  function Resp_channel#(a,w,o,i,op,acks,u) fn_gen_resp_pkt(Message#(a,w) m, Bit#(u) user)
    provisos(Add#(a__, 2, i),Add#(c__, 4, op),Add#(d__, 2, o),Add#(e__, 1, acks));
    let _r = Resp_channel{ opcode: zeroExtend(pack(m.msgtype)),
                          acksExpected:zeroExtend(m.acksExpected),
                          last: True,
                          source: zeroExtend(pack(m.src)), 
                          dest: signExtend(pack(m.dst)), 
                          address:m.address, 
                          corrupt:0,
                          data:m.cl,
                          user: user}; 
    return _r;
  endfunction
  
  function Message#(a,w) fn_from_resp_pkt(Resp_channel#(a,w,o,i,op,acks,u) r)
    provisos(Add#(a__, 2, i),Add#(c__, 4, op),Add#(d__, 2, o),Add#(e__, 1, acks));
    let m = Message {address: r.address,
                        msgtype: unpack(truncate(r.opcode)),
                        src : unpack(truncate(r.source)),
                        dst : unpack(truncate(r.dest)),
                        acksExpected : truncate(r.acksExpected),
                        cl : r.data};
    return m;
  endfunction
  
  function Message#(a,w) fn_from_fwd_pkt(Fwd_channel#(a,w,o,i,op,u) r)
    provisos(Add#(a__, 2, i), Add#(b__, 2, o), Add#(c__, 4, op)); 
    let m = Message {address: r.address,
                        msgtype: unpack(truncate(r.opcode)),
                        src : unpack(truncate(r.source)),
                        dst : unpack(truncate(r.dest)),
                        acksExpected : 0,
                        cl : r.data};
    return m;
  endfunction
  
  
  /*doc:module: */
  (*preempts="rl_resp_message_from_fabric,rl_fwd_message_from_fabric"*)
  (*preempts="rl_send_resp2_to_fabric,rl_send_resp_to_fabric"*)
  (*preempts="rl_send_io_req_to_fabric,rl_send_resp_to_fabric"*)
  module mkimem_controller#(parameter Integer id)(Ifc_imem_controller#(o,i))
    provisos(
      Add#(a__, 2, i),
      Add#(b__, 2, o));
      Ifc_imem imem <- mkimem(fromInteger(id));

    // This agent is responsible for sending out requests to the Directory
    // Since we are having the number of banks equal to the number of caches the source and
    // destinations ids are simply 2x number of caches
    Ifc_slc_master_agent#(`paddr, TMul#(`dwords,`dblocks),o,i,SizeOf#(MessageType),
                            TLog#(`NrCaches), 1) master <- mkslc_master_agent;
  
    /*doc:wire: */
    Wire#(Bit#(1)) wr_criticality <- mkWire();
    /*doc:rule: */
    rule rl_send_req_to_fabric;
      let req <- imem.read_mem_req.get;
      `logLevel( cc, 0, $format("CC[%2d]: Sending Request to Fabric:", id, fshow(req)))
      let _r = Req_channel{ opcode: zeroExtend(pack(GetS)),
                          len: 0,
                          size: req.burst_size,		                       
                          mode:0,
                          source: fromInteger(id), 
                          dest: 5, 
                          address:req.address, 
                          mask:'1,
                          data:?,
                          user: wr_criticality}; 
      master.i_req_channel.enq(_r);
    endrule

    /*doc:rule: */
    rule rl_resp_message_from_fabric;
      let x <- pop_o(master.o_resp_channel);
      `logLevel( cc, 0, $format("CC[%2d]: Received Response:",id,fshow(x)))
      imem.read_mem_resp.put(ICache_mem_readresp{data:x.data,err:False,last:True});
    endrule

    interface master_side = master.shaktilink_side;
      // -------------------- Cache related interfaces ------------//
    interface core_req = imem.core_req;
    interface core_resp = imem.core_resp;
    method cache_enable = imem.cache_enable;
    method cache_available = imem.cache_available;
    method Action ma_criticality(Bit#(1) c);
      wr_criticality <= c;
    endmethod
      // ---------------------------------------------------------//
      // - ---------------- TLB interfaces ---------------------- //
  `ifdef supervisor
    interface req_to_ptw = imem.req_to_ptw;
    interface response_frm_ptw= imem.response_frm_ptw;
    /*doc:method: method to receive the current satp csr from the core*/
    method ma_satp_from_csr = imem.ma_satp_from_csr;

    /*doc:method: method to recieve the current privilege mode of operation*/
    method ma_curr_priv = imem.ma_curr_priv;

    /*doc:method: method to receive the current values of the mstatus register*/
    `ifdef pmp
      method ma_pmp_cfg = imem.ma_pmp_cfg;
      method ma_pmp_addr = imem.ma_pmp_addr;
    `endif
  `endif
`ifdef perfmonitors
  `ifdef dcache
    method mv_icache_perf_counters = imem.mv_icache_perf_counters;
  `endif
  `ifdef supervisor
    method mv_itlb_perf_counters = imem.mv_itlb_perf_counters;
  `endif
`endif
  endmodule

//  (*synthesize*)
//  module mkcc_inst#(parameter Integer id)(Ifc_imem_controller#(`paddr, TDiv#(`linesize,8), TAdd#(TLog#(`NrCaches),1), 
//                          TAdd#(TLog#(`NrCaches),1),
//                      SizeOf#(MessageType),TLog#(`NrCaches),0));
//	  let  ifc();
//  	mk_imem_controller#(id) _temp(ifc);
//	  return (ifc);
//  endmodule
endpackage

