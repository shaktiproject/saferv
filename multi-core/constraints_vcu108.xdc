#-------------- MCS Generation ----------------------
set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN div-1  [current_design]
set_property BITSTREAM.CONFIG.BPI_SYNC_MODE Type1     [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE          [current_design]
set_property CONFIG_MODE BPI16                        [current_design]
set_property CFGBVS GND                               [current_design]
set_property CONFIG_VOLTAGE 1.8                       [current_design]

#-------------- Port-PIN mapping ----------------------

# sys_clk1_300_p - G31
#sysclk_125_p - BC9
set_property PACKAGE_PIN BC9 [get_ports c0_sys_clk_p]
set_property IOSTANDARD LVDS [get_ports c0_sys_clk_p]

# sys_clk2_300_n - F31
#sysclk_125_-n - BC8
set_property PACKAGE_PIN BC8 [get_ports c0_sys_clk_n]
set_property IOSTANDARD LVDS [get_ports c0_sys_clk_n]

#create_clock -period 3.333 [get_ports c0_sys_clk_p] # for 300 MHz clk
create_clock -period 8.000 [get_ports c0_sys_clk_p] # for 125 MHz clk
set_input_jitter [get_clocks -of_objects [get_ports c0_sys_clk_p]] 0.08

# CPU_RESET
set_property -dict { PACKAGE_PIN E36 IOSTANDARD LVCMOS12 } [get_ports {sys_rst}];

# UART ports - USB_UART_TX and USB_UART_RX
set_property IOB TRUE [get_ports uart_SOUT]
set_property IOB TRUE [get_ports uart_SIN]

set_property -dict {PACKAGE_PIN BE24 IOSTANDARD LVCMOS18 } [get_ports {uart_SOUT}];
set_property -dict {PACKAGE_PIN BC24 IOSTANDARD LVCMOS18 } [get_ports {uart_SIN}];
