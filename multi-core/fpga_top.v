/*
Copyright (c) 2018, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Arjun Menon , P.George
Details:
This module could be used as top module while synthesizing the SoC on an FPGA. This has 2 configs
that need to be independently controlled:
	1. SOFTRESET: Reset the clock divider & the SoC after cetrain number of predetermined
		      cycles. Otherwise, the CPU_Reset button needs to be pushed on the FPGA
		      for the SoC to start executing the code loaded in main memory.
	2. NOC: Used NoC as the interconnect. Otherwise, a crossbar is used for the interconnect.
--------------------------------------------------------------------------------------------------

*/
`define NOC
`define SOFTRESET

module fpga_top#( parameter AXI_ID_WIDTH   = 4, parameter AXI_ADDR_WIDTH = 31) (
  // ---- clock ports --------------//
  input                   c0_sys_clk_p,
  input                   c0_sys_clk_n,
  // ---- UART ports --------//
  input                   uart_SIN,
  output                  uart_SOUT,
  // ---- System Reset ------//
  input         sys_rst);                 //Active High Reset


  // ------ Wire Instantiations ------ //
  wire soc_clk;        // clock to the SoC
  wire sys_clk;        // clock from FPGA
  wire locked;         // Did the clock divider get locked to given frequency ?
  wire clk_div_reset;  // Reset to clock divider

`ifdef SOFTRESET
  reg  soft_reset;
  reg [19:0] cycles;

  assign clk_div_reset = soft_reset || sys_rst;
`else
  assign clk_div_reset = sys_rst;
`endif


  // -------------- Differential Clock to Single Ended Clock Converter --------------- //
  IBUFDS diff_clk (
                  .I(c0_sys_clk_p),
                  .IB(c0_sys_clk_n),
                  .O(sys_clk)
  );
  // ---------- Clock divider -----------//
  clk_wiz_0 clk_div (
                    .clk_in1(sys_clk),
                    .clk_out1(soc_clk),
                    .reset(clk_div_reset),
                    .locked(locked)
  );
  // ----------------------------------------- //

   // ---- Instantiating the C-class based multi-core SoC -------------//
`ifdef NOC
  mk_Soc_network_on_chip(
`else
  mk_Soc_crossbar soc_crossbar(
`endif
       // Main Clock and Reset to the SoC
       .CLK(soc_clk),
       .RST_N(locked),
       // UART port definitions
       .uart_io_SIN(uart_SIN),
       .uart_io_SOUT(uart_SOUT)
  );

`ifdef SOFTRESET
initial begin
  soft_reset = 1'b1;
  cycles = 20'b0;
end

always@(posedge sys_clk) begin
  if (cycles <= 300000) begin
    cycles <= cycles + 1;
    soft_reset <= 1'b1;
  end
  else begin
    soft_reset <= 1'b0;
  end
end
`endif

endmodule
