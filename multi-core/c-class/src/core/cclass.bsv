/*
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Neel Gala
Email id : neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package cclass;

  //=================== Interface and module for a cclass - master on the AXI4 fabric ============= //
  // project related imports
	import Semi_FIFOF:: *;
	import ShaktiLink_Types:: *;
	import ShaktiLink_Fabric:: *;
	import coherence_types :: * ;
  import riscv:: * ;
  import common_types:: * ;
  import FIFOF::*;
  import icache_types :: *;
  import dcache_types :: *;
  import Assert ::*;
  import imem::*;
  import dmem::*;
`ifdef supervisor
  `ifdef RV64
    import ptwalk_rv64::*;
  `else
    import ptwalk_rv32::*;
  `endif
`endif
  `include "common_params.bsv"
  `include "Logger.bsv"

  // package imports
	import Connectable 				:: *;
  import GetPut:: *;
  import BUtils::*;
  import Soc_crossbar_types :: * ;

`ifdef debug
  import debug_types::*;
`endif

  `ifdef supervisor
    typedef enum {None, IWalk, DWalk} PTWState deriving(Bits, Eq, FShow);
  `endif

  typedef enum {Request, Response} TxnState deriving(Bits, Eq, FShow);

  function Req_channel#(a,w,o,i,op,u) fn_gen_req_pkt(Message#(a,w) m, Bit#(o) id, Bit#(u) user)
    provisos(Add#(a__, 3, i),Add#(b__, 4, op));
    let _r = Req_channel{ opcode: zeroExtend(pack(m.msgtype)),
                          len: 0,
                          size: 3,		                       
                          tid:0,
                          source: zeroExtend(pack(id)), 
                          dest: signExtend(pack(m.dst)), 
                          address:m.address, 
                          mask:'1,
                          data:m.cl,
                          user: user}; 
    return _r;
  endfunction
  
  function Fwd_channel#(a,w,o,i,op,u) fn_gen_fwd_pkt(Message#(a,w) m, Bit#(u) user)
    provisos(Add#(a__, 3, i),Add#(b__, 3, o),Add#(c__, 4, op));
    let _r = Fwd_channel{ opcode: zeroExtend(pack(m.msgtype)),
                          source: zeroExtend(pack(m.src)), 
                          dest:signExtend(pack(m.dst)), 
                          address:m.address, 
                          mask:'1,
                          data:m.cl,
                          tid: 0,
                          user: user}; 
    return _r;
  endfunction
  
  function Resp_channel#(a,w,o,i,op,acks,u) fn_gen_resp_pkt(Message#(a,w) m, Bit#(u) user)
    provisos(Add#(a__, 3, i),Add#(c__, 4, op),Add#(d__, 3, o),Add#(e__, 2, acks));
    let _r = Resp_channel{ opcode: zeroExtend(pack(m.msgtype)),
                          acksExpected:zeroExtend(m.acksExpected),
                          last: True,
                          source: zeroExtend(pack(m.src)), 
                          dest: signExtend(pack(m.dst)), 
                          address:m.address, 
                          corrupt:0,
                          data:m.cl,
                          tid:0,
                          user: user}; 
    return _r;
  endfunction
  
  function Message#(a,w) fn_from_resp_pkt(Resp_channel#(a,w,o,i,op,acks,u) r)
    provisos(Add#(a__, 3, i),Add#(c__, 4, op),Add#(d__, 3, o),Add#(e__, 2, acks));
    let m = Message {address: r.address,
                        msgtype: unpack(truncate(r.opcode)),
                        src : unpack(truncate(r.source)),
                        dst : unpack(truncate(r.dest)),
                        acksExpected : truncate(r.acksExpected),
                        cl : r.data};
    return m;
  endfunction
  
  function Message#(a,w) fn_from_fwd_pkt(Fwd_channel#(a,w,o,i,op,u) r)
    provisos(Add#(a__, 3, i), Add#(b__, 3, o), Add#(c__, 4, op)); 
    let m = Message {address: r.address,
                        msgtype: unpack(truncate(r.opcode)),
                        src : unpack(truncate(r.source)),
                        dst : unpack(truncate(r.dest)),
                        acksExpected : 0,
                        cl : r.data};
    return m;
  endfunction

  interface Ifc_cclass_shl;
    interface Ifc_slc_master#(`paddr, TMul#(`dwords,`dblocks),FSize,FSize,SizeOf#(MessageType),
                            TLog#(`NrCaches), 1) master_side;
    interface Ifc_slc_slave#(`paddr, TMul#(`dwords,`dblocks),FSize,FSize,SizeOf#(MessageType),
                            TLog#(`NrCaches), 1) slave_side;
    interface Put#(Bit#(1)) sb_clint_msip;
    interface Put#(Bit#(1)) sb_clint_mtip;
    interface Put#(Bit#(64)) sb_clint_mtime;
    interface Put#(Bit#(1)) sb_externalinterrupt;
  `ifdef rtldump
    interface Get#(DumpType) io_dump;
  `endif
  `ifdef debug
    interface Hart_Debug_Ifc debug_server;
  `endif
  endinterface : Ifc_cclass_shl

  (*synthesize*)
  `ifdef supervisor
    (*preempts="dtlb_req_to_ptwalk, itlb_req_to_ptwalk"*)
    (*preempts="core_req_to_dmem, ptwalk_request_to_dcache"*)
  `endif
  (*preempts="rl_send_resp2_to_fabric,rl_send_resp_to_fabric"*)
  (*preempts="rl_send_io_req_to_fabric,rl_send_resp_to_fabric"*)
  (*preempts="rl_send_req_to_fabric, handle_imem_line_request"*)
  (*preempts="handle_imem_line_request, rl_send_io_req_to_fabric"*)
  module mkcclass_shl#(Bit#(`vaddr) resetpc, parameter Bit#(XLEN) hartid)(Ifc_cclass_shl);
    String core = "";
    let vaddr = valueOf(`vaddr);
    let paddr = valueOf(`paddr);
    Ifc_riscv riscv <- mkriscv(resetpc, hartid);
  `ifdef supervisor
  `ifdef RV64
    Ifc_ptwalk_rv64#(`asidwidth) ptwalk <- mkptwalk_rv64;
  `else
    Ifc_ptwalk_rv32#(`asidwidth) ptwalk <- mkptwalk_rv32;
  `endif
    Reg#(PTWState) rg_ptw_state <- mkReg(None);
  `endif
    // This agent is responsible for sending out requests to the Directory
    // Since we are having the number of banks equal to the number of caches the source and
    // destinations ids are simply 2x number of caches
    Ifc_slc_master_agent#(`paddr, TMul#(`dwords,`dblocks),FSize,FSize,SizeOf#(MessageType),
                            TLog#(`NrCaches), 1) master <- mkslc_master_agent;

    // This agent is responsible to communication to other caches.
    // Since we are having the number of banks equal to the number of caches the source and
    // destinations ids are simply 2x number of caches
    Ifc_slc_slave_agent#(`paddr, TMul#(`dwords,`dblocks),FSize,FSize,SizeOf#(MessageType),
                            TLog#(`NrCaches), 1) slave <- mkslc_slave_agent;

    let curr_priv = riscv.mv_curr_priv;

    // TODO debug
  `ifdef debug
    Reg#(Maybe#(Bit#(DXLEN))) rg_abst_response <- mkReg(tagged Invalid); // registered container for responses
    Reg#(Bool) rg_debug_waitcsr <- mkReg(False);
    let csr_response = riscv.mv_resp_to_core;
  `endif

	  Reg#(Bit#(TLog#(TDiv#(ELEN,8)))) rg_io_dmem_lower_addr_bits <- mkReg(0);
	  Reg#(Maybe#(Bit#(TLog#(TDiv#(ELEN,8))))) rg_fetch_lower_addr_bits <- mkReg(tagged Invalid);
	  Reg#(Maybe#(Bit#(TLog#(TDiv#(ELEN,8))))) rg_memory_lower_addr_bits <- mkReg(tagged Invalid);

    Ifc_imem imem <- mkimem(truncate(hartid));
	  mkConnection(imem.core_resp, riscv.inst_response); // imem integration

    rule connect_instruction_req;
      let req <- riscv.instr_req.get;
      imem.core_req.put(req);
    endrule

  `ifdef supervisor
    rule tlb_csr_info;
      imem.ma_satp_from_csr(riscv.mv_csr_satp);
      imem.ma_curr_priv(curr_priv);
    endrule
  `endif

	  rule handle_imem_line_request;
      let req <- imem.read_mem_req.get;
      let _r = Req_channel{ opcode: zeroExtend(pack(GetS)),
                          len: 0,
                          size: req.burst_size,		                       
                          source: truncate(hartid), 
                          dest: 8, 
                          address:req.address, 
                          mask:'1,
                          data:?,
                          tid: 1,
                          user: 0}; 
      master.i_req_channel.enq(_r);
	  	`logLevel( core, 1, $format("CORE[%2d] : IMEM Line Requesting ", hartid, fshow(req)))
	  endrule

	  rule handle_imem_line_resp(master.o_resp_channel.first.tid ==1);
      let x <- pop_o(master.o_resp_channel);
      imem.read_mem_resp.put(ICache_mem_readresp{data:x.data,err:False,last:True});
	  	`logLevel( core, 1, $format("CORE[%2d] : IMEM Line Response ", hartid, fshow(x)))
	  endrule
    rule drive_constants;
		  imem.cache_enable(unpack(riscv.mv_cacheenable[0]));
    endrule

    Ifc_dmem dmem <- mkdmem(truncate(hartid));
    rule core_req_to_dmem;
      let req <- riscv.memory_request.get;
      dmem.core_req.put(req);
      `logLevel( core, 1, $format("CORE[%2d] : Sending Request to DMEM:",hartid, fshow(req)))
    endrule
	  mkConnection(dmem.core_resp, riscv.memory_response); // dmem integration
  `ifdef supervisor
    rule dtlb_csr_info;
      dmem.ma_satp_from_csr(riscv.mv_csr_satp);
      dmem.ma_curr_priv(curr_priv);
      dmem.ma_mstatus_from_csr(riscv.mv_csr_mstatus);
    endrule
  `endif
    rule drive_dmem_enable;
		  dmem.cache_enable(unpack(riscv.mv_cacheenable[1]));
    endrule

    rule dirve_storebuffer_empyty;
      riscv.storebuffer_empty(dmem.storebuffer_empty);
      riscv.cache_is_available(dmem.cache_available);
    endrule

    rule initiate_store(tpl_2(riscv.initiate_store));
      dmem.perform_store(pack(tpl_1(riscv.initiate_store)));
    endrule
    rule connect_store_status;
      riscv.store_is_cached(True);
    endrule

    Reg#(Maybe#(Bit#(`paddr))) wr_write_req <- mkReg(tagged Invalid);

    /*doc:rule: */
    rule rl_send_req_to_fabric;
      let req <- dmem.mv_request_to_fabric.get;
      `logLevel( core, 0, $format("CORE[%2d]: Sending Request to Fabric:", hartid, fshow(req)))
      master.i_req_channel.enq(fn_gen_req_pkt(req,truncate(hartid), riscv.mv_criticality));
    endrule

    rule rl_send_io_req_to_fabric;
      let req <- dmem.mv_io_request_to_fabric.get;
      let _r = Req_channel{ opcode: req.read_write?zeroExtend(pack(PutM)):zeroExtend(pack(GetS)),
                          len: 0,
                          size: zeroExtend(req.size),		                       
                          source: truncate(hartid), 
                          dest: 9, 
                          address:req.addr, 
                          mask:'1,
                          data:zeroExtend(req.data),
                          tid: 0,
                          user: 0}; 
      master.i_req_channel.enq(_r);
      `logLevel( core, 0, $format("CORE[%2d]: Sending IO Request to Fabric: Addr:%h", hartid,req.addr))
    endrule

    rule rl_send_resp_to_fabric;
      let resp <- dmem.mv_response_to_fabric.get;
      `logLevel( core, 0, $format("CORE[%2d]: Sending Response:",hartid,fshow(resp)))
      slave.i_resp_channel.enq(fn_gen_resp_pkt(resp, riscv.mv_criticality));
    endrule
    rule rl_send_resp2_to_fabric;
      let resp <- dmem.mv_response2_to_fabric.get;
      `logLevel( core, 0, $format("CORE[%2d]: Sending Response:",hartid,fshow(resp)))
      slave.i_resp_channel.enq(fn_gen_resp_pkt(resp, riscv.mv_criticality));
    endrule

    /*doc:rule: */
    rule rl_resp_message_from_fabric(master.o_resp_channel.first.tid ==0);
      let x <- pop_o(master.o_resp_channel);
      `logLevel( core, 0, $format("CORE[%2d]: Received Response:",hartid,fshow(fn_from_resp_pkt(x))))
      dmem.mv_response_from_fabric.put(fn_from_resp_pkt(x));
    endrule

    /*doc:rule: */
    rule rl_fwd_message_from_fabric;
      let x <- pop_o(master.o_fwd_channel);
      `logLevel( core, 0, $format("CORE[%2d]: Received Fwd:",hartid,fshow(fn_from_fwd_pkt(x))))
      dmem.mv_fwd_from_fabric.put(fn_from_fwd_pkt(x));
    endrule

`ifdef supervisor
    rule csrs_to_ptwalk;
      ptwalk.satp_from_csr.put(riscv.mv_csr_satp);
      ptwalk.curr_priv.put(curr_priv);
      ptwalk.mstatus_from_csr.put(riscv.mv_csr_mstatus);
    endrule

  `ifdef pmp
    rule connect_pmp_to_imem;
      imem.pmp_cfg(riscv.mv_pmp_cfg);
      imem.pmp_addr(riscv.mv_pmp_addr);
    endrule

    rule connect_pmp_to_dmem;
      dmem.pmp_cfg(riscv.mv_pmp_cfg);
      dmem.pmp_addr(riscv.mv_pmp_addr);
    endrule
  `endif

    rule itlb_req_to_ptwalk(rg_ptw_state == None);
      let req <- imem.request_to_ptw.get();
      ptwalk.from_tlb.put(req);
      rg_ptw_state <= IWalk;
    endrule

    rule ptwalk_resp_to_itlb(rg_ptw_state == IWalk);
      let resp <- ptwalk.to_tlb.get();
      imem.response_frm_ptw.put(resp);
      rg_ptw_state <= None;
    endrule
    rule dtlb_req_to_ptwalk(rg_ptw_state == None);
      let req <- dmem.req_to_ptw.get();
      ptwalk.from_tlb.put(req);
      rg_ptw_state <= DWalk;
    endrule

    rule ptwalk_resp_to_dtlb(rg_ptw_state == DWalk);
      let resp <- ptwalk.to_tlb.get();
      dmem.resp_from_ptw.put(resp);
      rg_ptw_state <= None;
    endrule

    rule ptwalk_request_to_dcache;
	  	let req <- ptwalk.request_to_cache.get;
      dmem.core_req.put(req);
    endrule
    mkConnection(dmem.ptw_resp, ptwalk.response_frm_cache);
    mkConnection(dmem.hold_req, ptwalk.hold_req);
`endif

`ifdef perfmonitors
    mkConnection(riscv.ma_icache_counters,imem.mv_icache_perf_counters);
    mkConnection(riscv.ma_dcache_counters,dmem.mv_dcache_perf_counters);
  `ifdef supervisor
    mkConnection(riscv.ma_itlb_counters,imem.mv_itlb_perf_counters);
    mkConnection(riscv.ma_dtlb_counters,dmem.mv_dtlb_perf_counters);
  `endif
`endif   

  `ifdef debug
    rule rl_wait_for_csr_response(rg_debug_waitcsr && !isValid(rg_abst_response));
      if (csr_response.hit) begin
        rg_abst_response <= tagged Valid csr_response.data;
        rg_debug_waitcsr <= False;
      end
      else
        rg_debug_waitcsr <= True;
    endrule
  `endif

    interface sb_clint_msip = interface Put
  	  method Action put(Bit#(1) intrpt);
        riscv.ma_clint_msip(intrpt);
      endmethod
    endinterface;
    interface sb_clint_mtip = interface Put
      method Action put(Bit#(1) intrpt);
        riscv.ma_clint_mtip(intrpt);
      endmethod
    endinterface;
    interface sb_clint_mtime = interface Put
  		method Action put (Bit#(64) c_mtime);
        riscv.ma_clint_mtime(c_mtime);
      endmethod
    endinterface;
    interface sb_externalinterrupt = interface Put
      method Action put(Bit#(1) intrpt);
        riscv.ma_set_external_interrupt(intrpt);
      endmethod
    endinterface;
    interface master_side = master.shaktilink_side;
    interface slave_side = slave.shaktilink_side;
    `ifdef rtldump
      interface io_dump = riscv.dump;
    `endif
  `ifdef debug
    interface debug_server = interface Hart_Debug_Ifc

      method Action   abstractOperation(AbstractRegOp cmd)if (!(isValid(rg_abst_response)) 
                                                              && !rg_debug_waitcsr );
        if(cmd.address < zeroExtend(14'h1000))begin // Explot address bits to optimize this filter
          riscv.ma_debug_access_csrs(cmd);
          if (csr_response.hit)
            rg_abst_response <= tagged Valid zeroExtend(csr_response.data);
          else
            rg_debug_waitcsr <= True;
        end
        else if(cmd.address < `ifdef spfpu 'h1040 `else 'h1020 `endif )begin
          let lv_resp <- riscv.debug_access_gprs(cmd);
          rg_abst_response <= tagged Valid zeroExtend(lv_resp);
        end
        else begin
          rg_abst_response <= tagged Valid zeroExtend(32'h00000000);
        end
      endmethod

      method ActionValue#(Bit#(DXLEN)) abstractReadResponse if (isValid(rg_abst_response) );
        rg_abst_response <= tagged Invalid;
        return validValue(rg_abst_response);
      endmethod

      method haltRequest = riscv.ma_debug_halt_request;

      method resumeRequest = riscv.ma_debug_resume_request;

      method dm_active = riscv.ma_debugger_available;

      method is_halted = riscv.mv_core_is_halted();

      method is_unavailable = ~riscv.mv_core_debugenable;

      method Action hartReset(Bit#(1) hart_reset_v); // Change to reset type // Signal TO Reset HART -Active HIGH
        noAction;
      endmethod

      method Bit#(1) has_reset;
        return 1;
      endmethod
    endinterface;
  `endif
  endmodule : mkcclass_shl

endpackage
