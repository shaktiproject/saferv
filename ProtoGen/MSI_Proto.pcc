# NrCaches 2

Network { Ordered fwd;    //FwdGetS, FwdGetM, Inv, PutAck
          Ordered resp; // Data, InvAck
          Ordered req;   //GetS, GetM, PutM
          };

Cache {
    State I;
    Data cl;
    int[0..NrCaches] acksReceived = 0;
    int[0..NrCaches] acksExpected = 0;
} set[NrCaches] Cacheline_state;

Directory {
    State I;
    Data cl;
    set[NrCaches] ID sv;
    ID owner;
} Dict;

Message fn_Request{};

Message fn_Ack{};

Message fn_Resp{
    Data cl;
};

Message fn_RespAck{
    Data cl;
    int[0..NrCaches] acksExpected;
};

Architecture Cacheline_state {

    Stable{I, S, M}

    // I ////////////////////////////////////////////////////////
    Process(I, Load, State){
        msg = fn_Request(GetS, ID, Dict.ID);
        req.send(msg);

        await{
            when GetS_Ack:
                cl=GetS_Ack.cl;
                State = S;
                break;
        }
    }

    Process(I, Store, State){
        msg = fn_Request(GetM, ID, Dict.ID);
        req.send(msg);
        acksReceived = 0;

        await{
            when GetM_Ack_D:
                cl=GetM_Ack_D.cl;
                State = M;
                break;

            when GetM_Ack_AD:
                acksExpected = GetM_Ack_AD.acksExpected;
                cl=GetM_Ack_D.cl;
                if acksExpected == acksReceived{
                    State = M;
                    break;
                }

                await{
                    when Inv_Ack:
                        acksReceived = acksReceived + 1;

                        if acksExpected == acksReceived{
                            State = M;
                            break;
                        }
                    }

            when Inv_Ack:
                acksReceived = acksReceived + 1;
        }
    }

    // S ////////////////////////////////////////////////////////
    Process(S, Load, S){}

    Process(S, Store, State){
        msg = fn_Request(Upgrade, ID, Dict.ID);
        req.send(msg);
        acksReceived = 0;

        await{
            when GetM_Ack_D:
                State = M;
                break;

            when GetM_Ack_AD:
                acksExpected = GetM_Ack_AD.acksExpected;

                if acksExpected == acksReceived{
                    State = M;
                    break;
                }

                await{
                    when Inv_Ack:
                        acksReceived = acksReceived + 1;

                        if acksExpected == acksReceived{
                            State = M;
                            break;
                        }
                    }

            when Inv_Ack:
                acksReceived = acksReceived + 1;
        }
    }

    Process(S, Evict, State){
        msg = fn_Request(PutS, ID, Dict.ID);
        req.send(msg);

        await{
            when Put_Ack:
                State = I;
                break;
        }
    }

    Process(S, Inv, I){
        msg = fn_Resp(Inv_Ack, ID, Inv.src, cl);
        resp.send(msg);
    }


    // M ////////////////////////////////////////////////////////
    Process(M, Load){
    }

    Process(M, Store, M){}

    Process(M, Fwd_GetM, I){
        msg = fn_Resp(GetM_Ack_D, ID, Fwd_GetM.src, cl);
        resp.send(msg);
    }

    Process(M, Fwd_GetS, S){
        msg = fn_Resp(GetS_Ack, ID, Fwd_GetS.src, cl);
        resp.send(msg);
        msg = fn_Resp(WB, ID, Dict.ID, cl);
        resp.send(msg);
    }

    Process(M, Evict, State){
        msg = fn_Resp(PutM, ID, Dict.ID, cl);
        req.send(msg);

        await{
            when Put_Ack:
                State = I;
                break;
        }
    }

}

Architecture Dict {

    Stable{I, S, M}

    // I ////////////////////////////////////////////////////////
    Process(I, GetS, S){
       sv.add(GetS.src);
       msg = fn_Resp(GetS_Ack, ID, GetS.src, cl);
       resp.send(msg);
    }

    Process(I, GetM, M){
        msg = fn_RespAck(GetM_Ack_AD, ID, GetM.src, cl, sv.count());
        resp.send(msg);
        owner = GetM.src;
    }

    // S ////////////////////////////////////////////////////////
    Process(S, GetS){
       sv.add(GetS.src);
       msg = fn_Resp(GetS_Ack, ID, GetS.src, cl);
       resp.send(msg);
    }

    Process(S, Upgrade){
       if sv.contains(Upgrade.src){
           sv.del(Upgrade.src);
           msg = fn_RespAck(GetM_Ack_AD, ID, Upgrade.src, cl, sv.count());
           resp.send(msg);
           State=M;
           break;
       } else {
           msg = fn_RespAck(GetM_Ack_AD, ID, Upgrade.src, cl, sv.count());
           resp.send(msg);
           State=M;
           break;
       }
       msg = fn_Ack(Inv, Upgrade.src, Upgrade.src);
       fwd.mcast(msg, sv);
       owner = Upgrade.src;
       sv.clear();
    }

    Process(S, PutS){
       msg = fn_Resp(Put_Ack, ID, PutS.src, cl);
       fwd.send(msg);
       sv.del(PutS.src);

       if sv.count() == 0{
            State=I;
            break;
       }
    }

    // M ////////////////////////////////////////////////////////
    Process(M, GetS){
       msg = fn_Request(Fwd_GetS, GetS.src, owner);
       fwd.send(msg);
       sv.add(GetS.src);
       sv.add(owner);

       await{
            when WB:
                if WB.src == owner{
                    cl = WB.cl;
                    State=S;
                }
       }

    }

    Process(M, GetM){
       msg = fn_Request(Fwd_GetM, GetM.src, owner);
       fwd.send(msg);
       owner = GetM.src;
    }

    Process(M, PutM){
       msg = fn_Ack(Put_Ack, ID, PutM.src);
       fwd.send(msg);
       sv.del(PutM.src);

       if owner == PutM.src{
            cl = PutM.cl;
            State=I;
       }
    }

}
